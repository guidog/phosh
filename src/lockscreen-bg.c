/*
 * Copyright (C) 2025 The Phosh Developers
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#define G_LOG_DOMAIN "phosh-lockscreen-bg"

#include "phosh-config.h"

#include "shell-priv.h"
#include "background-cache.h"
#include "background-image.h"
#include "lockscreen-bg.h"
#include "style-manager.h"
#include "util.h"

#include <gmobile.h>

#include <gdesktop-enums.h>

#include <gtk/gtk.h>

#define SCREENSAVER_SETTINGS "org.gnome.desktop.screensaver"
#define KEY_PICTURE_URI       "picture-uri"
#define KEY_PICTURE_OPTIONS   "picture-options"

/**
 * PhoshLockscreenBg:
 *
 * The lockscreen's background
 */

struct _PhoshLockscreenBg {
  PhoshLayerSurface     parent;

  GFile                *file;
  GFileMonitor         *monitor;     /* Monitors file */
  PhoshBackgroundImage *cached_bg_image;
  GdkPixbuf            *pixbuf;
  GCancellable         *cancel;

  GSettings            *settings;
  gboolean              configured;
  gboolean              use_background;
};
G_DEFINE_TYPE (PhoshLockscreenBg, phosh_lockscreen_bg, PHOSH_TYPE_LAYER_SURFACE)


static void
update_image (PhoshLockscreenBg *self)
{
  int width, height;
  GdkPixbuf *pixbuf;

  if (!self->configured)
    return;

  width = phosh_layer_surface_get_configured_width (PHOSH_LAYER_SURFACE (self));
  height = phosh_layer_surface_get_configured_height (PHOSH_LAYER_SURFACE (self));

  g_return_if_fail (width > 0 && height > 0);

  g_debug ("Scaling lockscreen background %p to %dx%d", self, width, height);

  g_clear_object (&self->pixbuf);
  pixbuf = phosh_background_image_get_pixbuf (self->cached_bg_image);
  self->pixbuf = phosh_utils_pixbuf_scale_to_min (pixbuf, width, height);

  gtk_widget_queue_draw (GTK_WIDGET (self));
}


static void
on_background_cache_fetch_ready (GObject *source_object, GAsyncResult *res, gpointer data)
{
  g_autoptr (GError) err = NULL;
  g_autoptr (PhoshBackgroundImage) image = NULL;
  PhoshLockscreenBg *self = PHOSH_LOCKSCREEN_BG (data);
  PhoshBackgroundCache *cache = PHOSH_BACKGROUND_CACHE (source_object);

  image = phosh_background_cache_fetch_finish (cache, res, &err);
  if (!image) {
    phosh_async_error_warn (err, "Failed to load background image");
    return;
  }

  g_assert (PHOSH_IS_LOCKSCREEN_BG (self));
  g_assert (PHOSH_IS_BACKGROUND_CACHE (cache));

  g_set_object (&self->cached_bg_image, image);
  update_image (self);
}


static void
load_background (PhoshLockscreenBg *self)
{
  PhoshBackgroundCache *cache = phosh_background_cache_get_default ();

  g_cancellable_cancel (self->cancel);
  g_clear_object (&self->cancel);
  self->cancel = g_cancellable_new ();

  phosh_background_cache_fetch_async (cache,
                                      self->file,
                                      self->cancel,
                                      on_background_cache_fetch_ready,
                                      self);
}



static void
on_file_changed (PhoshLockscreenBg *self,
                 GFile             *file,
                 GFile             *other_file,
                 GFileMonitorEvent  event_type,
                 GFileMonitor      *monitor)
{
  PhoshBackgroundCache *cache = phosh_background_cache_get_default ();

  if (event_type != G_FILE_MONITOR_EVENT_CHANGES_DONE_HINT)
    return;

  g_warning ("Lockscreen background file changed, clearing cache");
  phosh_background_cache_remove (cache, self->file);

  load_background (self);
}


static void
monitor_file (PhoshLockscreenBg *self)
{
  g_autoptr (GError) err = NULL;
  g_clear_object (&self->monitor);

  if (!self->file)
    return;

  self->monitor = g_file_monitor_file (self->file, G_FILE_MONITOR_NONE, NULL, &err);
  if (!self->monitor) {
    g_autofree char *uri = g_file_get_uri (self->file);

    g_warning ("Failed to setup file monitor for %s: %s", uri, err->message);
    return;
  }

  g_signal_connect_object (self->monitor, "changed",
                           G_CALLBACK (on_file_changed),
                           self,
                           G_CONNECT_SWAPPED);
}


static void
set_image (PhoshLockscreenBg *self, const char *uri)
{
  g_autoptr (GFile) file = NULL;
  PhoshBackgroundCache *cache = phosh_background_cache_get_default ();

  if (gm_str_is_null_or_empty (uri)) {
    g_clear_object (&self->file);
    monitor_file (self);
    return;
  }

  if (!g_str_has_prefix (uri, "/") && !g_str_has_prefix (uri, "file:///")) {
    g_warning ("Invalid lockscreen uri %s", uri);
    return;
  }

  file = g_file_new_for_uri (uri);

  if (self->file && !phosh_util_file_equal (self->file, file))
    phosh_background_cache_remove (cache, self->file);

  g_set_object (&self->file, file);
  monitor_file (self);
  load_background (self);
}


static void
on_picture_uri_changed (PhoshLockscreenBg *self)
{
  g_autofree char *uri = NULL;

  uri = g_settings_get_string (self->settings, KEY_PICTURE_URI);
  set_image (self, uri);
}


static void
phosh_lockscreen_bg_configured (PhoshLayerSurface *layer_surface)
{
  PhoshLockscreenBg *self = PHOSH_LOCKSCREEN_BG (layer_surface);

  PHOSH_LAYER_SURFACE_CLASS (phosh_lockscreen_bg_parent_class)->configured (layer_surface);

  self->configured = TRUE;
  on_picture_uri_changed (self);
}


static gboolean
phosh_lockscreen_bg_draw (GtkWidget *widget, cairo_t *cr)
{
  GtkStyleContext *context;
  PhoshLockscreenBg *self = PHOSH_LOCKSCREEN_BG (widget);
  int x = 0, y = 0, width, height;

  g_return_val_if_fail (PHOSH_IS_LOCKSCREEN_BG (self), GDK_EVENT_PROPAGATE);

  if (!self->configured)
    return GDK_EVENT_PROPAGATE;

  cairo_save (cr);
  context = gtk_widget_get_style_context (GTK_WIDGET (self));

  width = gtk_widget_get_allocated_width (GTK_WIDGET (self));
  height = gtk_widget_get_allocated_height (GTK_WIDGET (self));
  gtk_render_background (context, cr, 0, 0, width, height);

  if (self->pixbuf && self->use_background) {
    gdk_cairo_set_source_pixbuf (cr, self->pixbuf, x, y);
    cairo_paint (cr);
  }

  cairo_restore (cr);

  return GDK_EVENT_PROPAGATE;
}


static void
on_theme_name_changed (PhoshLockscreenBg *self, GParamSpec *pspec, PhoshStyleManager *style_manager)
{
  g_assert (PHOSH_IS_LOCKSCREEN_BG (self));
  g_assert (PHOSH_IS_STYLE_MANAGER (style_manager));

  self->use_background = !phosh_style_manager_is_high_contrast (style_manager);
  gtk_widget_queue_draw (GTK_WIDGET (self));
}


static void
phosh_lockscreen_bg_finalize (GObject *object)
{
  PhoshLockscreenBg *self = PHOSH_LOCKSCREEN_BG (object);

  g_cancellable_cancel (self->cancel);
  g_clear_object (&self->cancel);

  g_clear_object (&self->monitor);
  g_clear_object (&self->file);
  g_clear_object (&self->settings);
  g_clear_object (&self->cached_bg_image);
  g_clear_object (&self->pixbuf);

  G_OBJECT_CLASS (phosh_lockscreen_bg_parent_class)->finalize (object);
}


static void
phosh_lockscreen_bg_class_init (PhoshLockscreenBgClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  PhoshLayerSurfaceClass *layer_surface_class = PHOSH_LAYER_SURFACE_CLASS (klass);

  object_class->finalize = phosh_lockscreen_bg_finalize;

  widget_class->draw = phosh_lockscreen_bg_draw;

  layer_surface_class->configured = phosh_lockscreen_bg_configured;

  gtk_widget_class_set_css_name (widget_class, "phosh-lockscreen-bg");
}


static void
phosh_lockscreen_bg_init (PhoshLockscreenBg *self)
{
  PhoshStyleManager *style_manager;
  GDesktopBackgroundStyle bg_style;

  style_manager = phosh_shell_get_style_manager (phosh_shell_get_default ());
  self->use_background = !phosh_style_manager_is_high_contrast (style_manager);
  g_signal_connect_object (style_manager,
                           "notify::theme-name",
                           G_CALLBACK (on_theme_name_changed),
                           self,
                           G_CONNECT_SWAPPED);

  self->settings = g_settings_new (SCREENSAVER_SETTINGS);
  bg_style = g_settings_get_enum (self->settings, KEY_PICTURE_OPTIONS);

  if (bg_style != G_DESKTOP_BACKGROUND_STYLE_NONE) {
    g_signal_connect_object (self->settings,
                             "changed::" KEY_PICTURE_URI,
                             G_CALLBACK (on_picture_uri_changed),
                             self,
                             G_CONNECT_SWAPPED);
    on_picture_uri_changed (self);
  }
}


PhoshLockscreenBg *
phosh_lockscreen_bg_new (struct zwlr_layer_shell_v1 *layer_shell, struct wl_output *wl_output)
{
  return g_object_new (PHOSH_TYPE_LOCKSCREEN_BG,
                       "layer-shell", layer_shell,
                       "wl-output", wl_output,
                       "anchor", (ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                                  ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT),
                       "layer", ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY,
                       "kbd-interactivity", FALSE,
                       "exclusive-zone", -1,
                       "namespace", "phosh lockscreen background",
                       NULL);
}
